package com.qa.hello;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.velocity.VelocityConfig;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

import com.qa.hello.dao.VetDAO;
import com.qa.hello.dao.VetDAOImpl;

@Configuration
@ComponentScan("com.qa")
public class SpringConfig {

	@Bean 
	public VetDAO dao(){
		return new VetDAOImpl();
	}
	
	@Bean
	public String getGreeting(){
		return "Hello world!";
	}

	@Bean 
	public VelocityConfig velocityConfig() {
		VelocityConfigurer c = new VelocityConfigurer();
		c.setResourceLoaderPath("/WEB-INF/templates/");
		return c;
	}

	@Bean
	public VelocityViewResolver viewResolver() {
		VelocityViewResolver resolver = new VelocityViewResolver();
		resolver.setPrefix("");
		resolver.setCache(true);
		resolver.setSuffix(".vm");
		resolver.setOrder(1);
		return resolver;
	}
}

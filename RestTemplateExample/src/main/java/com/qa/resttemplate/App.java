package com.qa.resttemplate;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class App implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication sa = new SpringApplication(App.class);
		sa.setWebEnvironment(false);
		sa.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		  RestTemplate restTemplate = new RestTemplate();
	      Animal a = restTemplate.getForObject("http://localhost:8080/", Animal.class);
	      System.out.println(a);
	      
	      List<Animal> animals = Arrays.asList(restTemplate.getForObject("http://localhost:8080/list", Animal[].class));
	      
	      for (Animal animal: animals) {
	    	  System.out.println(animal);
	      }
	      
	      Animal newOne = new Animal("Franky", "Badger", 7, "Black and white");
	      Object r = restTemplate.postForObject("http://localhost:8080/receive", newOne, String.class);
	      System.out.println(r);
	     
	}
}

